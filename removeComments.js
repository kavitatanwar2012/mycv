/*
*  file: minify.js
*  content: functions to minify js files
*/

var removeComments = function(inputFilePath) {
	var isCommentStarted = false;
	inputFilePath = __dirname+"/staticfiles/js/dev/CV.js";
	
	output = "";

	var lineReader = require('line-reader');
	lineReader.eachLine(inputFilePath, function(line, last) {
		// console.log(line);
	  	line = line.trim();

	  	if ( !isCommentStarted && hasMultiLineCommentStartedAndNotFinished(line)) {
	  		
	  		// console.log(lineWithoutComments(line));
	  		if (lineWithoutComments(line) != "")
	  			output += lineWithoutComments(line);
	  		
	  		isCommentStarted = true;
	  		return;
	  	}

	  	if (isCommentStarted) {
	  		if (!hasMultiLineCommentFinished(line)) 
	  			return;
	  	
	  		isCommentStarted = false;
	  		return;
	  	}

	  	//If empty then the whole line is a comment
	  	if (lineWithoutComments(line) == "") {
	  		return;
	  	}

	  	// console.log(lineWithoutComments(line));
	  	output += lineWithoutComments(line);

	  	if (last) {
	  		console.log(output);
	  	}
	});
	console.log(output);
	return output;
}
/*
* Reads an input line and removes comments from it. Will return "" if only comments are present in the string
* Returns a line without comments in it.
*/
function lineWithoutComments(str) {

	//Check if line starts with comment. Return empty string if true.
	if (str.startsWith("//") || str.startsWith("/*")) return "";
	
	var commentFreeLine = "";
	for (var i=0; i<str.length; i++) {
		//ignore comment check in single and double quotes (strings)
		if (str[i] == "\"") {
			commentFreeLine += str[i];
			i++;
			while (i<str.length && str[i] != "\"") {
				commentFreeLine += str[i];
				i++;
			}
		}
		if (str[i] == "'") {
			commentFreeLine += str[i];
			i++;
			while (i<str.length && str[i] != "'") {
				commentFreeLine += str[i];
				i++;
			}
		}

		if (str[i] == "/" && str[i+1] == "/")
			break;

		if (str[i] == "/" && str[i+1] == "*") {
			i++;
			while (i<str.length-1 && !(str[i] == "*" && str[i+1] == "/")) {
				i++;
			}

			//If while loop stops because end of multiline comment found, skip those characters
			if (i<str.length-1)
				i = i+2;
		}

		if (i >=str.length)
			break;

		commentFreeLine += str[i];
	}
	return commentFreeLine;
}

/*
* Checks to see if, for a given line, a multiline comment began, but didn't close within the same line.
* Returns true if multiline comment is left unclosed
*/
function hasMultiLineCommentStartedAndNotFinished(str) {
	for (var i=0; i< str.length-1; i++) {
		if (str[i] == "/" && str[i+1] == "*") {
			i++;
			while (i<str.length-1 && !(str[i] == "*" && str[i+1] == "/")) {
				i++;
			}
		}
		if (i >=str.length-1)
			return true;
	}
	return false;
}

/*
* Checks a line to see if it has end of multiline string present in it.
*/

function hasMultiLineCommentFinished(str) {
	if (str.startsWith("*/")) return true;

	commentFreeLine = "";
	for (var i=0; i<str.length; i++) {

		//ignore comment check in single and double quotes (strings)
		if (str[i] == "\"") {

			i++;
			while (i<str.length && str[i] != "\"") {
				commentFreeLine += str[i];
				i++;
			}
		}
		if (str[i] == "'") {

			i++;
			while (i<str.length && str[i] != "'") {
				commentFreeLine += str[i];
				i++;
			}
		}

		if (str[i] == "*" && str[i+1] == "/") {
			return true;
		}

	}
	return false;
}
//export the function removeComments
module.exports = removeComments;
