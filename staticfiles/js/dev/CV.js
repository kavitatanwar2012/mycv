/*
*  file: CV.js
*  authore: Kavita Singh Tanwar
*  date: 18/06/2016
*/
//functions to change my Image
function myImage(){
	document.getElementById('myimage').src='me1.jpg';
}
function myImageAgain(){
	document.getElementById('myimage').src='me.jpg';
}

//function to change text color on mouse move
function textColorAbout1(){
	document.getElementById('about').style.color='blue';
}
function textColorAbout2(){
	document.getElementById('about').style.color='black';
}
function textColorComp1(){
	document.getElementById('competencies').style.color='blue';
}
function textColorComp2(){
	document.getElementById('competencies').style.color='black';
}

//Function to display details from education
function displayEducation(){
	document.getElementById('education').style.display= 'block';
	document.getElementById('introduction').style.display= 'none';
	document.getElementById('projects').style.display= 'none';
	document.getElementById('cocurricular').style.display= 'none';
	document.getElementById('contact').style.display= 'none';
}
//Function to display details from introduction
function displayIntroduction(){
	document.getElementById('introduction').style.display= 'block';
	document.getElementById('education').style.display= 'none';
	document.getElementById('projects').style.display= 'none';
	document.getElementById('cocurricular').style.display= 'none';
	document.getElementById('contact').style.display= 'none';
}
//Function to display details from projects
function displayProjects(){
	document.getElementById('education').style.display= 'none';
	document.getElementById('introduction').style.display= 'none';
	document.getElementById('projects').style.display= 'block';
	document.getElementById('cocurricular').style.display= 'none';
	document.getElementById('contact').style.display= 'none';
}
//Function to display details from co-curricular activities
function displayCocurricular(){
	document.getElementById('education').style.display= 'none';
	document.getElementById('introduction').style.display= 'none';
	document.getElementById('projects').style.display= 'none';
	document.getElementById('cocurricular').style.display= 'block';
	document.getElementById('contact').style.display= 'none';
}
//Function to display details from contact
function displayContact(){
	document.getElementById('education').style.display= 'none';
	document.getElementById('introduction').style.display= 'none';
	document.getElementById('projects').style.display= 'none';
	document.getElementById('cocurricular').style.display= 'none';
	document.getElementById('contact').style.display= 'block';
}