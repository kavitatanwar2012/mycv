/*
*  file: usingUglify.js.js
*  content: call and store minified js files using Uglify
*/


var express = require('express');
var fs = require('fs');
var UglifyJS = require("uglify-js");

var app = express();
app.use(express.static(__dirname + "/staticfiles/css"));
app.use(express.static(__dirname + "/staticfiles/images"));

var result = UglifyJS.minify(__dirname+"/staticfiles/js/dev/CV.js");

fs.writeFile(__dirname+"/staticfiles/js/prod/CV.js", result.code, function (err){
	 if(err) {
     console.log(err);
	   } else {
	  	//Serve minified js on success
   	 app.use(express.static(__dirname + "/staticfiles/js/prod"));
     console.log("JS minification successful and saved:", 'CV.js');
       }      
  });

app.get('/', function (req, res) {
	res.sendFile(__dirname+ '/MyCV.html');
});

app.listen(3000, function () {
  console.log('Server listening on port 3000!');
});


function writeToFile(outputFilePath, str) {
	outputFilePath = __dirname+"/staticfiles/js/prod/CV.js";
	fs.writeFile(outputFilePath, str, function (err){
	  if(err) {
	    console.log(err);
	  } else {
	  	//Serve minified js on success
	  	// app.use(express.static(__dirname + "/staticfiles/js/prod"));
	    console.log("JS minification successful and saved:", 'CV.js');
	  }      
	});
}

